﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnManager : MonoBehaviour
{
    public struct Turn
    {
        public GameObject pieceMoving;
        public Vector2 actualPosition;
        public Vector2 newPosition;
        public GameObject eatenPiece;
    }

    public List<Turn> turns;


    private Piece.Color colorTurn;

    public Board board;

    //public static TurnManager instance;

    //private void Awake()
    //{
    //    if (instance == null)
    //    {
    //        instance = this;
    //    }
    //    else
    //    {
    //        Destroy(gameObject);
    //    }
    //}

    // Start is called before the first frame update
    void Start()
    {
        colorTurn = Piece.Color.White;
        turns = new List<Turn>();
        board = GetComponentInParent<Board>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool IsMyTurn(Piece.Color color)
    {
        if(color == colorTurn)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public Piece.Color WhoseTurn()
    {
        return colorTurn;
    }

    public void NextTurn(Piece pieceMoving, Vector2 actualPosition, Vector2 newPosition, Piece eatenPiece)
    {
        Turn nextTurn=new Turn();

        nextTurn.pieceMoving = pieceMoving.gameObject;
        nextTurn.actualPosition = actualPosition;
        nextTurn.newPosition = newPosition;
        if (eatenPiece != null) nextTurn.eatenPiece = eatenPiece.gameObject;

        //colorTurn = (colorTurn == Piece.Color.White) ? Piece.Color.Black : Piece.Color.White;

        turns.Add(nextTurn);

        board.CheckForChecks(colorTurn);

        colorTurn = board.OtherColor(colorTurn);

        board.CheckForCheckmate();


        eatenPiece = null;

        AudioManager.instance.MoveSound();
    }

    public void TakeBack()
    {
        Debug.Log("1. TakeBack");
        if (turns.Count == 0) return;

        Turn lastTurn = turns[turns.Count - 1];
        
        Vector2 pos;

        if (turns.Count == 1)
        {
            pos = lastTurn.pieceMoving.GetComponent<Piece>().startPosition;
        }
        else
        {
            pos = lastTurn.actualPosition;
        }

        lastTurn.pieceMoving.GetComponent<Piece>().MovePiece(pos);

        if (lastTurn.eatenPiece != null)
        {
            lastTurn.eatenPiece.gameObject.SetActive(true);
        }

        turns.RemoveAt(turns.Count - 1);

        colorTurn = board.OtherColor(colorTurn);

        board.ErasePossibleMoves();
        Debug.Log("2. TakeBack");

    }
}
