﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Square : MonoBehaviour
{
    private Color ownColor;
    public Color darkColor;
    private SpriteRenderer spriteRenderer;
    private Board board;

    // Start is called before the first frame update
    void Start()
    {
        board = GetComponentInParent<Board>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        ownColor = spriteRenderer.color;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeColorSelected()
    {
        spriteRenderer.color = darkColor;
    }

    public void ChangeColorNormal()
    {
        spriteRenderer.color = ownColor;
    }

    public void SelectSquare()
    {

        //Debug.Log("SelectSquare");
        board.SelectSquare(transform, this);
    }
}
