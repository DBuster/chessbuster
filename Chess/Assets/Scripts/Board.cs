﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using Mirror;
using System;


public class Board : NetworkBehaviour
{
    public List<Piece> pieces;
    private Piece selectedPiece;
    private Piece selectedPieceRaw;
    private Square selectedSquare;
    private Piece eatenPieceRaw;
    public bool changingTurn;
    private Vector2 previousPosRaw;
    public Transform whitePieceContainer;
    public Transform BlackPieceContainer;

    public GameObject possibleMovePrefab;
    public GameObject possibleEatPrefab;
    public Transform possibleMovesContainer;
    private List<GameObject> possibleMoveObjects;
    private List<Vector2> possibleMoveSquares;

    public GameObject whitePawnPanel;
    public GameObject blackPawnPanel;
    public GameObject checkmatePawnPanel;
    public Text checkmatePawnColor;


    private Piece pawnConvert;
    private GameObject pawnEatPiece;
    private Vector2 pawnPreviousPosition;

    private TurnManager turnManager;

    [SerializeField] private GameObject board = null;
    private static event Action<Vector2, Vector2> OnMove;

    //public static Board instance;

    private void Awake()
    {
        //if (instance == null)
        //{
        //    instance = this;
        //}
        //else
        //{
        //    Destroy(gameObject);
        //}

        pieces = new List<Piece>();
    }

    

    #region NETWORK
    public override void OnStartAuthority()
    {
        board.SetActive(true);

        OnMove += MovePiece;
    }

    [ClientCallback]
    private void OnDestroy()
    {
        if (!hasAuthority) { return; }

        OnMove -= MovePiece;
    }


    [Command]
    private void CmdSendMove(Vector2 actualPos, Vector2 newPos)
    {
        // Validate message
        RpcHandleMessage(actualPos, newPos);
    }

    [ClientRpc]
    private void RpcHandleMessage(Vector2 actualPos, Vector2 newPos)
    {
        OnMove?.Invoke(actualPos, newPos);
    }
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        possibleMoveObjects = new List<GameObject>();
        turnManager = GetComponentInChildren<TurnManager>();
    }

    public bool IsOnBoard(Vector2 position)
    {
        if (position.x < 1 || position.x > 8 || position.y < 1 || position.y > 8)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public Piece WhatIsOnSquare(Vector2 position)
    {
        Piece piece = pieces.Find(a => a.position == position);

        if (piece == null)
        {
            return null;
        }
        else
        {
            return piece;
        }
    }

    private void DrawPossibleMoves(List<Vector2> position)
    {
        Vector2[] positionArray = position.ToArray();

        foreach (Vector2 pos in positionArray)
        {
            Piece piece = pieces.Find(a => a.position == pos);

            GameObject obj;

            if (piece != null)
            {
                obj = Instantiate(possibleEatPrefab, new Vector3(pos.x, pos.y, 0), Quaternion.identity, possibleMovesContainer);
            }
            else
            {
                obj = Instantiate(possibleMovePrefab, new Vector3(pos.x, pos.y, 0), Quaternion.identity, possibleMovesContainer);
            }
            possibleMoveObjects.Add(obj);
        }

        selectedSquare.ChangeColorSelected();
    }

    public void ErasePossibleMoves()
    {
        foreach (GameObject obj in possibleMoveObjects)
        {
            Destroy(obj);
        }

        possibleMoveSquares.RemoveRange(0, possibleMoveSquares.Count);

        if(selectedSquare != null)selectedSquare.ChangeColorNormal();
    }

    [Client]
    public void SelectSquare(Transform posTransform, Square square)
    {
        Vector2 pos = new Vector2(posTransform.position.x, posTransform.position.y);
        //Debug.Log("Select square: "+pos);

        if (pawnConvert != null)
        {
            CancelPawnToEighthRaw();
            return;
        }

        if (selectedPiece != null)
        {
            
            Piece newSelectedPiece = pieces.Find(a => a.position == pos);

            if (newSelectedPiece != null && turnManager.IsMyTurn(newSelectedPiece.color))
            {
                ErasePossibleMoves();

                if (selectedPiece != newSelectedPiece)
                {
                    selectedPiece = newSelectedPiece;

                    selectedSquare = square;
                    possibleMoveSquares = newSelectedPiece.PossibleMoves(/*false*/);

                    DrawPossibleMoves(possibleMoveSquares);
                }
                else
                {
                    selectedSquare = null;
                    selectedPiece = null;
                }
                               



            }
            else
            {
                if (possibleMoveSquares.Contains(pos))
                {

                    if (newSelectedPiece != null)
                    {
                        float eighthRaw;

                        if (selectedPiece.color == Piece.Color.White)
                        {
                            eighthRaw = 8f;
                        }
                        else
                        {
                            eighthRaw = 1f;
                        }

                        pieces.Remove(newSelectedPiece);

                        if (selectedPiece.piece == Piece.Pieces.Pawn && pos.y == eighthRaw)
                        {
                            pawnEatPiece = newSelectedPiece.gameObject;
                            pawnEatPiece.SetActive(false);
                        }
                        else
                        {
                            Destroy(newSelectedPiece.gameObject);
                        }
                        //}

                    }
                    CmdSendMove(selectedPiece.position, pos);
                    //MovePiece(selectedPiece, pos);
                }

                ErasePossibleMoves();

                selectedPiece = null;

                selectedSquare = null;
            }
        }
        else
        {
            Piece piece = pieces.Find(a => a.position == pos);

            if (piece != null && turnManager.IsMyTurn(piece.color))
            {
                selectedPiece = piece;
                selectedSquare = square;

                possibleMoveSquares = piece.PossibleMoves(/*false*/);

                DrawPossibleMoves(possibleMoveSquares);
            }
        }
    }

    public void AddPiece(Piece piece)
    {
        pieces.Add(piece);
    }

    #region PawnOn8thRaw

    public void PawnToEighthRaw(Piece pawn, Vector2 previousPosition)
    {
        pawnConvert = pawn;
        pawnPreviousPosition = previousPosition;

        if (pawn.color == Piece.Color.White)
        {
            whitePawnPanel.SetActive(true);
        }
        else
        {
            blackPawnPanel.SetActive(true);
        }
    }

    public void CancelPawnToEighthRaw()
    {
        pawnConvert.MovePiece(pawnPreviousPosition);

        pawnConvert = null;
        pawnPreviousPosition = Vector2.zero;

        if(pawnEatPiece != null)
        {
            pawnEatPiece.SetActive(true);
            pieces.Add(pawnEatPiece.GetComponent<Piece>());
        }

        if (whitePawnPanel.activeSelf)
        {
            whitePawnPanel.SetActive(false);
        }

        if (blackPawnPanel.activeSelf)
        {
            blackPawnPanel.SetActive(false);
        }
    }

    public void ConvertPawnTo(GameObject newPiece)
    {
        GameObject newPieceObject = Instantiate(newPiece, pawnConvert.transform.position, Quaternion.identity, (newPiece.GetComponent<Piece>().color==Piece.Color.White?whitePieceContainer:BlackPieceContainer));

        pieces.Remove(pawnConvert);
        pieces.Add(newPieceObject.GetComponent<Piece>());

        Destroy(pawnConvert.gameObject);
        
        if (whitePawnPanel.activeSelf)
        {
            whitePawnPanel.SetActive(false);
        }

        if (blackPawnPanel.activeSelf)
        {
            blackPawnPanel.SetActive(false);
        }
    }

    #endregion

    public bool CheckForChecks(Piece.Color color)
    {
        //Debug.Log("CheckForChecks 1");
        bool isChecked = false;

        Piece.Color colorToCheck = OtherColor(color);

        List<Piece> colorPieces = pieces.FindAll(a => a.color == color);

        //Debug.Log("CheckForChecks 2. Pieces: " + colorPieces.Count);

        Piece enemyKing = pieces.Find(a => a.color == OtherColor(color) && a.piece == Piece.Pieces.King);

        List<Vector2> possibleMoves=new List<Vector2>();

        foreach (Piece colorPiece in colorPieces)
        {
            possibleMoves = colorPiece.PossibleRawMoves();

            //List<Vector2> checkMoves = new List<Vector2>();

            if (possibleMoves.Count != 0)
            {
                foreach (Vector2 possibleMove in possibleMoves)
                {
                    if (possibleMove == enemyKing.position)
                    {
                        Debug.Log("Jaque al " + OtherColor(color) + " en " + colorPiece.position + " de: " + colorPiece.ToString());

                        isChecked = true;
                    }
                }
            }
        }
        //Debug.Log("CheckForChecks 3");

        return isChecked;
    }

    public void CheckForCheckmate()
    {
        Debug.Log("1");
        int possibleMoves = 0;

        Piece.Color colorTurn = turnManager.WhoseTurn();

        List<Piece> piecesTurn = pieces.FindAll(a => a.color == colorTurn);

        foreach (Piece piece in piecesTurn)
        {
            possibleMoves += piece.PossibleMoves(/*true*/).Count;

        }

        if (possibleMoves == 0)
        {
            checkmatePawnColor.text = OtherColor(colorTurn).ToString();

            checkmatePawnPanel.SetActive(true);
            Debug.Log("JAQUE MATE");

        }
        Debug.Log("2. possibleMoves= "+ possibleMoves);
    }

    public Piece.Color OtherColor(Piece.Color _color)
    {
        Piece.Color result;

        result = (_color == Piece.Color.Black) ? Piece.Color.White : Piece.Color.Black;

        return result;
    }

    public void MovePiece(Vector2 actualPos, Vector2 newPos)
    {
        Piece piece = pieces.Find(a => a.position == actualPos);
        //Vector2 actualPos = piece.position;
        Piece eatenPiece=pieces.Find(a => a.position == newPos);

        if (eatenPiece != null)
        {
            pieces.Remove(eatenPiece);
            eatenPiece.gameObject.SetActive(false);
        }

        changingTurn = true;
        piece.MovePiece(newPos);
        changingTurn = false;

        turnManager.NextTurn(piece, actualPos, newPos, eatenPiece);
    }

    public void MovePieceRaw(Piece piece, Vector2 newPos)
    {
        //Vector2 actualPos = piece.position;
        selectedPieceRaw = piece;
        previousPosRaw = piece.position;
        eatenPieceRaw = pieces.Find(a => a.position == newPos);

        if (eatenPieceRaw != null)
        {
            pieces.Remove(eatenPieceRaw);
            eatenPieceRaw.gameObject.SetActive(false);
        }

        piece.MovePiece(newPos);
    }

    public void TakeBackRaw()
    {
        Vector2 actualPos = selectedPieceRaw.position;

        if (eatenPieceRaw != null)
        {
            pieces.Add(eatenPieceRaw);
            eatenPieceRaw.gameObject.SetActive(true);
        }

        selectedPieceRaw.MovePiece(previousPosRaw);
    }
}
