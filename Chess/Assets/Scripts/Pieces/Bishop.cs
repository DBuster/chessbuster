﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bishop : Piece
{

    // Update is called once per frame
    void Update()
    {
        
    }

    public override List<Vector2> PossibleRawMoves()
    {
        //Debug.Log("1");
        List<Vector2> possibleMovesList = new List<Vector2>();

        Vector2 nextPosition;

        bool findPiece = false;

        float xMove;
        float yMove;

        for (int i = -1; i < 2; i = i + 2)
        {
            //Debug.Log("For 1");
            xMove = i;

            for (int j = -1; j < 2; j = j + 2)
            {
                //Debug.Log("For 2");
                nextPosition = position;
                yMove = j;
                findPiece = false;

                while (!findPiece)
                {
                    //Debug.Log("While");
                    nextPosition += new Vector2(xMove, yMove);
                    findPiece = CheckSquare(nextPosition, possibleMovesList);
                }

            }
        }

        return possibleMovesList;
    }
}
