﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piece : MonoBehaviour
{
    //public int[] positionInt;
    public Vector2 position;
    public Vector2 startPosition;
    public Board board;

    public enum Pieces
    {
        None, Pawn, Bishop, Knight, Rook, Queen, King
    };

    public Pieces piece;

    public enum Color
    {
        White, Black
    };

    public Color color;

    //[HideInInspector] public bool isWhite;


    private void Start()
    {
        StartMethod();
    }

    // Start is called before the first frame update
    public void StartMethod()
    {
        position = new Vector2(transform.position.x, transform.position.y);
        startPosition = position;
        board = GetComponentInParent<Board>();

        board.AddPiece(this);
    }

    public virtual List<Vector2> PossibleRawMoves()
    {
        List<Vector2> possibleMoves = new List<Vector2>();

        return possibleMoves;
    }

    public List<Vector2> PossibleMoves(/*bool checkingForMate*/)
    {
        List<Vector2> possibleMoves = PossibleRawMoves();
        List<Vector2> unvalidMoves = new List<Vector2>();

        Vector2 actualPosition = position;

        //Debug.Log("possibleMoves.Count "+possibleMoves.Count);

        foreach (Vector2 rawPosition in possibleMoves)
        {

            Piece previousPiece = board.pieces.Find(a => a.position == rawPosition);
            //if (checkingForMate)
            //{
            //}
            //else 
            //{ 
            board.MovePieceRaw(this, rawPosition);

                if (board.CheckForChecks(board.OtherColor(color)))
                {
                    unvalidMoves.Add(rawPosition);
                }

                if (previousPiece != null)
                {
                    previousPiece.gameObject.SetActive(true);
                }

            board.TakeBackRaw();
            //}
        }

        foreach (Vector2 unvalidMove in unvalidMoves)
        {
            possibleMoves.Remove(unvalidMove);
        }

        return possibleMoves;
    }

    public bool CheckSquare(Vector2 position, List<Vector2> possibleMovesList)
    {
        if (board.IsOnBoard(position))
        {
            Piece piece = board.WhatIsOnSquare(position);

            if (piece == null)
            {
                possibleMovesList.Add(position);
                return false;
            }
            else
            {
                if (piece.color != color)
                {
                    possibleMovesList.Add(position);
                }
                return true;
            }
        }
        else
        {
            return true;
        }
    }


    public virtual void MovePiece(Vector2 pos)
    {
        transform.position = new Vector3(pos.x, pos.y, 0f);
        position = pos;

    }
}
