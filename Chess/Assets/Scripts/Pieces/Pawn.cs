﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pawn : Piece
{
    private float isWhiteInt;

    private void Start()
    {
        StartMethod();

        isWhiteInt = (color == Color.White) ? 1 : -1;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log("Board.instance.pieces.Count" + board.pieces.Count);
            int i = 0;
            foreach (Piece piece in board.pieces)
            {
                Debug.Log(i+". " + piece.position);
                i++;
            }
            
        }
    }

    public List<Vector2> CheckPossibleMoves()
    {
        List<Vector2> possibleMoves = PossibleRawMoves();

        if (possibleMoves.Count == 0)
        {
            Debug.Log("No possible moves");

        }
        else
        {
            for (int i = 0; i < possibleMoves.Count; i++)
            {
                Debug.Log("Possible move " + (i+1) + ": (" + possibleMoves[i].x + ", " + possibleMoves[i].y + "). ");
            }

        }

        return possibleMoves;
    }

    public override List<Vector2> PossibleRawMoves()
    {
        List<Vector2> possibleMovesList = new List<Vector2>();

        CheckPawnEat(possibleMovesList);
        CheckPawnMove(possibleMovesList);

        return possibleMovesList;
    }

    private void CheckPawnEat(List<Vector2> possibleMovesListVector)
    {
        Vector2 nextPosition = new Vector2(position.x + 1, position.y + isWhiteInt);

        if (board.IsOnBoard(nextPosition))
        {
            if (board.WhatIsOnSquare(nextPosition) != null)
            {
                if (board.WhatIsOnSquare(nextPosition).color != color)
                {
                    possibleMovesListVector.Add(nextPosition);
                }
            }
        }

        nextPosition.x = position.x - 1;

        if (board.IsOnBoard(nextPosition))
        {
            if (board.WhatIsOnSquare(nextPosition) != null)
            {
                if (board.WhatIsOnSquare(nextPosition).color != color)
                {
                    possibleMovesListVector.Add(nextPosition);
                }
            }
        }

    }

    private void CheckPawnMove(/*List<int[]> _possibleMovesList, */ List<Vector2> possibleMovesListVector)
    {
        Vector2 nextPosition = new Vector2(position.x, position.y + isWhiteInt);
        //int[] nextPosition = new int[2];
        //nextPosition[0] = position[0];
        //nextPosition[1] = position[1] + isWhiteInt;
        if (board.IsOnBoard(nextPosition))
        {
            if (board.WhatIsOnSquare(nextPosition) == null)
            {
                //_possibleMovesList.Add(nextPosition);
                possibleMovesListVector.Add(nextPosition);
                //Debug.Log("1: ("+nextPosition[0]+", "+nextPosition[1]+")");
                //Debug.Log("1_possibleMovesList: (" + _possibleMovesList[0][0] + ", " + _possibleMovesList[0][1] + "). "+ _possibleMovesList.Count.ToString());
                
            }
        }

        float firstRaw = 4.5f - 2.5f * (float)isWhiteInt;

        if(position.y == firstRaw)
        {
            nextPosition.y = position.y + 2 * isWhiteInt;

            if (board.IsOnBoard(nextPosition))
            {
                if (board.WhatIsOnSquare(nextPosition) == null)
                {
                    //_possibleMovesList.Add(nextPosition);
                    possibleMovesListVector.Add(nextPosition);
                    //Debug.Log("2: (" + nextPosition[0] + ", " + nextPosition[1] + ")");
                    //Debug.Log("2_possibleMovesList: (" + _possibleMovesList[0][0] + ", " + _possibleMovesList[0][1] + "). " + _possibleMovesList.Count.ToString());
                }
            }
        }
    }

    public override void MovePiece(Vector2 pos)
    {
        if (pos.y == 8f)
        {
            board.PawnToEighthRaw(this, position);
        }

        base.MovePiece(pos);
    }
}
