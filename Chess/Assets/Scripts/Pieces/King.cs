﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class King : Piece
{
    private bool hasMoved;

    public override List<Vector2> PossibleRawMoves()
    {
        List<Vector2> possibleMovesList = new List<Vector2>();

        Vector2 nextPosition;

        float xMove;
        float yMove;

        for (int i = -1; i < 2; i = i + 2)
        {
            xMove = i;

            for (int j = -1; j < 2; j = j + 2)
            {
                nextPosition = position;
                yMove = j;

                nextPosition += new Vector2(xMove, yMove);
                bool findPiece = CheckSquare(nextPosition, possibleMovesList);


            }
        }

        for (int i = -1; i < 2; i++)
        {
            xMove = i;

            for (int j = -1; j < 2; j = j + 2)
            {
                nextPosition = position;


                yMove = (xMove == 0) ? j : 0;

                nextPosition += new Vector2(xMove, yMove);
                bool findPiece = CheckSquare(nextPosition, possibleMovesList);


                if (xMove != 0) j = 2;
            }
        }

        CheckCastle(possibleMovesList);

        return possibleMovesList;
    }

    private void CheckCastle(List<Vector2> possibleMovesList)
    {
        if (hasMoved) return;

        List<Piece> rooks = board.pieces.FindAll(a => a.piece == Piece.Pieces.Rook && a.color == color);

        List<Rook> unmovedRooks = new List<Rook>();

        foreach (Piece r in rooks)
        {
            Rook rook = r.GetComponent<Rook>();

            if(!rook.hasMoved)unmovedRooks.Add(rook);
        }

        List<Vector2> possibleCastles = new List<Vector2>();

        foreach (Rook rook in unmovedRooks)
        {
            List<Vector2> possibleRookMoves = rook.PossibleRawMoves();

            float side = Mathf.Sign(rook.position.x - position.x);

            if (possibleRookMoves.Contains(position + new Vector2(side, 0f)))
            {
                possibleMovesList.Add(position + new Vector2(2 * side, 0f));
            }
        }
    }

    public override void MovePiece(Vector2 pos)
    {
        if (board.changingTurn)
        {
            if (!hasMoved) hasMoved = true;

            if ((pos - position).magnitude == 2)
            {
                float side = Mathf.Sign(pos.x - position.x);

                Piece rook = board.pieces.Find(a => a.position.x == 4.5f + 3.5f * side && a.position.y == position.y);

                rook.MovePiece(position + new Vector2(side, 0f));
            }
        }

        base.MovePiece(pos);
    }
}
