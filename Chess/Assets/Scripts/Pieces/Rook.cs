﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rook : Piece
{
    public bool hasMoved;

    public override List<Vector2> PossibleRawMoves()
    {
        List<Vector2> possibleMovesList = new List<Vector2>();

        Vector2 nextPosition;

        bool findPiece = false;

        float xMove;
        float yMove;

        for (int i = -1; i < 2; i++)
        {
            xMove = i;

            for (int j = -1; j < 2; j = j + 2)
            {
                nextPosition = position;

                //if (xMove == 0)
                //{
                //    yMove = j;
                //}
                //else
                //{
                //    yMove = 0;
                //}

                yMove = (xMove == 0) ? j : 0;

                findPiece = false;

                //Debug.Log("xMove: " + xMove +". yMove: " + yMove);

                while (!findPiece)
                {
                    nextPosition += new Vector2(xMove, yMove);
                    findPiece = CheckSquare(nextPosition, possibleMovesList);
                }

                if (xMove != 0)j=2;
            }
        }

        return possibleMovesList;
    }

    public override void MovePiece(Vector2 pos)
    {
            base.MovePiece(pos);

        if (board.changingTurn)
        {
            if (!hasMoved) hasMoved = true;
        }
    }
}
