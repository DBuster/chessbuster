﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knight : Piece
{
    public override List<Vector2> PossibleRawMoves()
    {
        List<Vector2> possibleMovesList = new List<Vector2>();

        Vector2 nextPosition;

        bool check = false;

        float xMove;
        float yMove;

        for (int i = -2; i < 3; i++)
        {
            xMove = i;

            for (int j = -2; j < 3; j++)
            {
                yMove = j;

                check = (xMove == 0 || yMove == 0 || Mathf.Abs(xMove) == Mathf.Abs(yMove)) ? false : true;

                nextPosition = position;

                //Debug.Log("xMove: " + xMove + ". yMove: " + yMove);

                nextPosition += new Vector2(xMove, yMove);

                if(check) check = CheckSquare(nextPosition, possibleMovesList);
            }
        }

        return possibleMovesList;
    }
}
